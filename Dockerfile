# FROM tensorflow/tensorflow:latest-gpu-py3
FROM gpueater/ubuntu16-rocm-1.9.211-tensorflow-1.11.0
MAINTAINER sleepless-se "sleepless.se@gmail.com"

COPY . /app

RUN apt update && apt install -y git wget curl tar zip unzip python-pil python-lxml vim nginx python3-pip zlib1g-dev libssl-dev libsm6 libxext6 libxrender-dev

RUN wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tgz && \
    tar -xzf Python-3.7.1.tgz && \
    cd Python-3.7.1 && \
    ./configure && \
    make && \
    make install

RUN pip3 install uwsgi flask supervisor &&  pip3 install -r /app/requirements.txt
RUN pip3 install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl


WORKDIR /app
RUN cp etc/nginx.conf /etc/nginx/nginx.conf
RUN cp etc/uwsgi.ini /etc/uwsgi.ini
RUN cp etc/supervisord.conf /etc/supervisord.conf

RUN wget https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/yolo.h5

EXPOSE 80 5000
CMD ["/usr/local/bin/supervisord"]